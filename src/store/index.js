import Vue from 'vue';
import Vuex from 'vuex';
import Api from '../services/api';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        resultOrganizations: [],
        resultUsers: [],
        resultTickets: [],

        u_organization: [],
        u_user: [],
        u_assignTickets: [],
        u_submittedTicket: [],

        t_assigneeUser: [],
        t_submittedUser: [],
        t_orgData: [],
        t_ticketData: [],
    },
    mutations: {
        SET_ORGANIZATIONS(state, resultOrganizations) {
            state.resultOrganizations = resultOrganizations;
        },
        SET_USERS(state, resultUsers) {
            state.resultUsers = resultUsers;
        },
        SET_TICKETS(state, resultTickets) {
            state.resultTickets = resultTickets;
        },
        RESET_RESULTS(state) {
            state.resultOrganizations = [];
            state.resultUsers = [];
            state.resultTickets = [];
            state.u_organization = [],
            state.u_user = [],
            state.u_assignTickets = [],
            state.u_submittedTicket = [],
            state.t_assigneeUser = [],
            state.t_submittedUser = [],
            state.t_orgData = [],
            state.t_ticketData = []
        },

        SET_ORGANIZATION(state, u_organization) {
            state.u_organization = u_organization;
        },
        SET_USER(state, u_user) {
            state.u_user = u_user;
        },

        SET_ASSIGN_TIKETS(state, u_assignTickets) {
            state.u_assignTickets = u_assignTickets;
        },

        SET_SUBMITTED_TIKETS(state, u_submittedTicket) {
            state.u_submittedTicket = u_submittedTicket;
        },

        SET_ASSIGNEE_USER(state, t_assigneeUser) {
            state.t_assigneeUser = t_assigneeUser;
        },

        SET_SUBMITTED_USER(state, t_submittedUser) {
            state.t_submittedUser = t_submittedUser;
        },
        SET_TICKET(state, t_ticketData) {
            state.t_ticketData = t_ticketData;
        },

    },
    actions: {
        async search(context, data) {
            var feild_array = Object.entries(data.keyword).map(([key, value]) => ({ key, value }));

            var query = ""
            feild_array.forEach(element => {
                if (element.value) {
                    let str = "&" + element.key + "=" + element.value
                    query += str
                }
            });

            if (data.type == "org") {

                let orgData = await Api().get('organizations?' + query)
                if (orgData.data.length > 0) {
                    let userData = await Api().get('users?organization_id=' + orgData.data[0]._id)
                    let ticketData = await Api().get('tickets?organization_id=' + orgData.data[0]._id)
                    context.commit('SET_ORGANIZATIONS', orgData.data);
                    context.commit('SET_USERS', userData.data);
                    context.commit('SET_TICKETS', ticketData.data);
                }

            } else if (data.type == "user") {

                let userData = await Api().get('users?' + query)
                if (userData.data.length > 0) {
                    let assigneeTktData = await Api().get('tickets?assignee_id=' + userData.data[0]._id)
                    let submitedTktData = await Api().get('tickets?submitter_id=' + userData.data[0]._id)
                    let orgData = await Api().get('organizations?_id=' + userData.data[0].organization_id)
                    context.commit('SET_ORGANIZATION', orgData.data);
                    context.commit('SET_USER', userData.data);
                    context.commit('SET_ASSIGN_TIKETS', assigneeTktData.data);
                    context.commit('SET_SUBMITTED_TIKETS', submitedTktData.data);
                }

            } else if (data.type == "ticket") {


                let ticketData = await Api().get('tickets?' + query)

                if (ticketData.data.length > 0) {
                    let submitterUserData = await Api().get('users?_id=' + ticketData.data[0].submitter_id)
                    let assigneeUserData = await Api().get('users?_id=' + ticketData.data[0].assignee_id)
                    let orgData = await Api().get('organizations?_id=' + ticketData.data[0].organization_id)
                    context.commit('SET_ORGANIZATION', orgData.data);
                    context.commit('SET_ASSIGNEE_USER', assigneeUserData.data);
                    context.commit('SET_SUBMITTED_USER', submitterUserData.data);
                    context.commit('SET_TICKET', ticketData.data);
                }

            }



        },

    },
    modules: {}
}); 
import Vue from 'vue';
import VueRouter from 'vue-router';
import Organization from '../views/Organization.vue';
import User from '../views/User.vue';
import Home from '../views/Home.vue';
import Ticket from '../views/Ticket.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        children:[
            {
                path: 'organization',
                name: 'organization',
                component: Organization
            },
            {
                path: 'user',
                name: 'user',
                component: User
            },
            {
                path: 'ticket',
                name: 'ticket',
                component: Ticket
            }
        ]
    },
    
];

const router = new VueRouter({
    routes
});

export default router;